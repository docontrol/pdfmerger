﻿using System.Runtime.InteropServices;

namespace PdfMergerLib
{
    /// <summary>
    /// Initialization properties for use with IMergeFunctions.
    /// </summary>
    [ComVisible(false)]
    public class InitializationProperties
    {
        /// <summary>
        /// Configuration file path, e.g. C:\merge\config.xml or "C:\merge dir\config.xml".
        /// </summary>
        public virtual string ConfigurationFilePath { get; set; }

        /// <summary>
        /// Writes basic output log to this file. Use with Verbose mode to write a detailed log file.
        /// </summary>
        public virtual string LogFilePath { get; set; }

        /// <summary>
        /// Set to true to write an output log file. Requires the log file path to be set as well.
        /// </summary>
        public virtual bool WriteLogFile { get; set; }

        /// <summary>
        /// Apply GZIP compression to the log file (if any). The .gz extension will be appended automatically. Default is no compression.
        /// </summary>
        public virtual bool CompressLogFile { get; set; }

        /// <summary>
        /// Print verbose output. Default is no output aside from a final OK message. Use with log file to write detailed log.
        /// </summary>
        public virtual bool Verbose { get; set; }

        /// <summary>
        /// Keep bookmarks already present in the pdf files to merge. By default all current bookmarks are removed.
        /// </summary>
        public virtual bool KeepCurrentPdfBookmarks { get; set; }

        /// <summary>
        /// Insert blank pages for duplex printing purposes. Pdf files with an uneven number of pages will have have a blank page appended before the next file is appended.
        /// </summary>
        public virtual bool InsertBlankPagesForDuplexPrinting { get; set; }
    }
}
