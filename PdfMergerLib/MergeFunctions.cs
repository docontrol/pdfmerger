﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using iText.Kernel;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Navigation;
using iText.Pdfa;
using Serilog;
using Serilog.Sinks.File.GZip;

namespace PdfMergerLib
{
    /// <summary>
    /// </summary>
    [Guid("E320F6B0-CA93-4D4E-94AE-C1FEDAD1152E")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class MergeFunctions : IMergeFunctions
    {
        /// <summary>
        /// The general merge properties to use.
        /// </summary>
        private MergeProperties _mergeProperties;

        /// <summary>
        /// Arguments passed to the application.
        /// </summary>
        private InitializationProperties _initializationProperties;

        /// <summary>
        /// Collection of bookmarks to append to the output file.
        /// </summary>
        private PdfOutline _bookmarks;

        /// <summary>
        /// The last bookmark appended.
        /// </summary>
        private PdfOutline _lastBookmark;

        /// <summary>
        /// The level of the last bookmark appended.
        /// </summary>
        private int _lastBookmarkLevel;
        
        /// <summary>
        /// The level of the last bookmark appended.
        /// </summary>
        private bool _loggerCreatedInternally;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MergeFunctions()
        {
            _mergeProperties = null;
            _bookmarks = null;
            _lastBookmark = null;
            _lastBookmarkLevel = -1;
            _loggerCreatedInternally = false;
        }

        /// <summary>
        /// Default destructor.
        /// </summary>
        ~MergeFunctions()
        {
            UnInitialize();
        }

        /// <summary>
        /// Initialize using the supplied properties.
        /// </summary>
        /// <param name="initializationProperties">Initialization properties, e.g. program arguments in console application mode.</param>
        [ComVisible(false)]
        public void Initialize(InitializationProperties initializationProperties)
        {
            _initializationProperties = initializationProperties;
            _mergeProperties = null;
            _bookmarks = null;
            _lastBookmark = null;
            _lastBookmarkLevel = -1;
        }

        /// <summary>
        /// Initialize using the supplied properties.
        /// </summary>
        /// <param name="configurationFilePath">Configuration file path, e.g. C:\merge\config.xml or "C:\merge dir\config.xml".</param>
        /// <param name="logFilePath">Writes basic output log to this file. Use with Verbose mode to write a detailed log file.</param>
        /// <param name="compressLogFile">Apply GZIP compression to the log file (if any). The .gz extension will be appended automatically. Default is no compression.</param>
        /// <param name="verbose">Print verbose output. Default is no output aside from a final OK message. Use with log file to write detailed log.</param>
        /// <param name="keepCurrentPdfBookmarks">Keep bookmarks already present in the pdf files to merge. By default all current bookmarks are removed.</param>
        /// <param name="insertBlankPagesForDuplexPrinting">Insert blank pages for duplex printing purposes. Pdf files with an uneven number of pages will have have a blank page appended before the next file is appended.</param>
        public void Initialize(string configurationFilePath, string logFilePath, bool compressLogFile, bool verbose, bool keepCurrentPdfBookmarks,
                               bool insertBlankPagesForDuplexPrinting)
        {
            _initializationProperties = new InitializationProperties()
            {
                ConfigurationFilePath = configurationFilePath,
                LogFilePath = logFilePath,
                WriteLogFile = !string.IsNullOrWhiteSpace(logFilePath),
                CompressLogFile = compressLogFile,
                Verbose = verbose,
                KeepCurrentPdfBookmarks = keepCurrentPdfBookmarks,
                InsertBlankPagesForDuplexPrinting = insertBlankPagesForDuplexPrinting
            };
            _mergeProperties = null;
            _bookmarks = null;
            _lastBookmark = null;
            _lastBookmarkLevel = -1;

            // Setup logging internally when necessary
            if (_initializationProperties.WriteLogFile &&
                (Log.Logger == null || Log.Logger.GetType().Name == "SilentLogger"))
            {
                logFilePath = _initializationProperties.CompressLogFile ? _initializationProperties.LogFilePath + ".gz" : _initializationProperties.LogFilePath;
                var logConfig = new LoggerConfiguration().MinimumLevel.Verbose();
                logConfig.WriteTo.File(logFilePath, hooks: _initializationProperties.CompressLogFile ? new GZipHooks() : null);
                Log.Logger = logConfig.CreateLogger();

                // Log the application name and version.
                Log.Information(System.Reflection.Assembly.GetExecutingAssembly().GetName().FullName);
                _loggerCreatedInternally = true;
            }
        }

        /// <summary>
        /// Un-initialize.
        /// </summary>
        public void UnInitialize()
        {
            if (!_loggerCreatedInternally)
            {
                return;
            }

            // Release ressources used by the logger
            try
            {
                Log.CloseAndFlush();
                _loggerCreatedInternally = false; // Successfully released
            }
            catch (ObjectDisposedException)
            {
            }
        }

        /// <summary>
        /// Merges pdf files based on an xml configuration file.
        /// </summary>
        /// <returns></returns>
        public bool MergeUsingConfigurationFile()
        {
            if (_initializationProperties == null)
            {
                Log.Error("Initialize was not called prior to MergeUsingConfigurationFile.");
                return false;
            }

            // Load the xml file describing the merge process
            var xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.Load(_initializationProperties.ConfigurationFilePath);
                Log.Information("{ConfigurationFilePath} was loaded.", _initializationProperties.ConfigurationFilePath);
            }
            catch (XmlException e)
            {
                Log.Error(e, "MergeUsingConfigurationFile threw an exception while loading the configuration file.");
                return false;
            }
            
            // Get the required (and some optional properties)
            if (!GetMergeProperties(_initializationProperties.ConfigurationFilePath, xmlDocument.DocumentElement))
            {
                return false;
            }

            // Get the first element to merge
            var xmlNode = xmlDocument.SelectSingleNode("/merge/element");
            if (null == xmlNode)
            {
                Log.Error("Unsupported configuration file structure [element].");
                return false;
            }

            // Verify sure that the first element has attributes
            if (null == xmlNode.Attributes)
            {
                Log.Error("Unsupported configuration file structure [element].");
                return false;
            }

            // Get the Pdf/A conformance level of the first file to merge.
            PdfAConformanceLevel conformanceLevel = null;
            if (_mergeProperties.EnablePdfA &&
                !GetPdfAConformanceLevel(xmlNode.Attributes.GetNamedItem("path")?.Value, out conformanceLevel))
            {
                return false;
            }

            // Start a new pdf document and set main properties
            var pdfDocument = CreateOutputDocument(conformanceLevel);
            
            // Create PdfMerger instance
            var pdfMerger = new iText.Kernel.Utils.PdfMerger(pdfDocument);
            
            // Initial page count
            var nextPage = 1;

            // Add each element in the xml file
            do
            {
                // Get element attributes
                var attributes = xmlNode.Attributes;
                if (null == attributes)
                {
                    Log.Error("Unsupported configuration file structure [element].");
                    return false;
                }

                // Get element file path
                var elementFilePath = attributes.GetNamedItem("path")?.Value;

                // If the path not empty we add the file otherwise we only add a bookmark
                var pagesAdded = 0;
                if (!string.IsNullOrWhiteSpace(elementFilePath))
                {
                    // Make sure that file path is absolute.
                    if (!elementFilePath.Contains("\\") &&
                        !string.IsNullOrWhiteSpace(_mergeProperties.WorkingDirectory))
                    {
                        elementFilePath = Path.Combine(_mergeProperties.WorkingDirectory, elementFilePath);
                    }
                    Log.Information("Appending file: {ElementFilePath}", elementFilePath);

                    // Now append the document
                    if (!AppendDocument(elementFilePath, pdfMerger, out pagesAdded))
                    {
                        return false;
                    }

                    // If the document has an uneven number of pages, append a
                    // blank page if the output is to be used for duplex printing.
                    if (_initializationProperties.InsertBlankPagesForDuplexPrinting
                        && pagesAdded % 2 != 0)
                    {
                        Log.Information("Appending 1 blank page.");
                        pdfDocument.AddNewPage();
                        pagesAdded++;
                    }
                }

                // Append bookmark if enabled
                if (_mergeProperties.EnableBookmarks)
                {
                    // Get attributes required for bookmarks
                    if (!int.TryParse(attributes.GetNamedItem("level")?.Value, out var bookmarkLevel))
                    {
                        Log.Error("Unsupported configuration file structure [level].");
                        return false;
                    }

                    // Now append the bookmark
                    var bookmarkTitle = attributes.GetNamedItem("title")?.Value;
                    if (!AppendBookmark(bookmarkTitle, bookmarkLevel, nextPage))
                    {
                        return false;
                    }
                }
                
                nextPage += pagesAdded;
                xmlNode = xmlNode.NextSibling;

            } while (null != xmlNode);
            
            // Close the document
            pdfDocument.Close();

            Log.Information("Files were successfully merged.");
            UnInitialize();
            return true;
        }

        /// <summary>
        /// Creates the output document and sets various properties,
        /// e.g. author, title, Pdf/A conformance level.
        /// </summary>
        /// <param name="conformanceLevel">Optional Pdf/A conformance level.</param>
        /// <returns>The created PdfDocument object or null.</returns>
        private PdfDocument CreateOutputDocument(PdfAConformanceLevel conformanceLevel)
        {
            // Create the Document
            PdfDocument pdfDocument;

            // Merge Pdf/A compliant files
            if (_mergeProperties.EnablePdfA)
            {
                if (null == conformanceLevel)
                {
                    Log.Error("The PDF/A conformance level is required.");
                    return null;
                }

                // The ICC color scheme to use for Pdf/A merging
                var iccColorScheme = Convert.FromBase64String(
                    "AAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf//");

                pdfDocument = new PdfADocument(new PdfWriter(_mergeProperties.DestinationFile), conformanceLevel, new PdfOutputIntent
                                                   ("Custom", "", "http://www.color.org", "sRGB IEC61966-2.1", new MemoryStream(iccColorScheme)));
            }
            else
            {
                // Merge ordinary pdf files
                pdfDocument = new PdfDocument(new PdfWriter(_mergeProperties.DestinationFile, new WriterProperties().SetFullCompressionMode(true)));
            }

            // Set as tagged
            pdfDocument.SetTagged();

            // Display document title instead of file name in the title bar
            var viewerPreferences = pdfDocument.GetCatalog().GetViewerPreferences();
            if (null == viewerPreferences)
            {
                viewerPreferences = new PdfViewerPreferences();
                pdfDocument.GetCatalog().SetViewerPreferences(viewerPreferences);
            }
            viewerPreferences.SetDisplayDocTitle(true);

            // Trigger the initial Adobe Acrobat view "Page and Bookmarks"
            // that will open the bookmarks pane for easy navigation.
            if (_mergeProperties.EnableBookmarks)
            {
                // There is actually a PdfViewerPreferences for the same,
                // but for some reason it does not seem to work. This
                // workaround seems to however.
                pdfDocument.GetCatalog().Put(PdfName.PageMode, PdfName.UseOutlines);
            }

            // Set various document properties
            var documentInfo = pdfDocument.GetDocumentInfo();
            documentInfo.SetAuthor(_mergeProperties.Author);
            documentInfo.SetTitle(_mergeProperties.Title);
            documentInfo.SetSubject(_mergeProperties.Subject);
            documentInfo.SetKeywords(_mergeProperties.Keywords);

            // Get bookmarks
            if (_mergeProperties.EnableBookmarks)
            {
                _bookmarks = pdfDocument.GetOutlines(false);
                _lastBookmark = _bookmarks;
            }

            Log.Information("Output document created.");
            return pdfDocument;
        }

        /// <summary>
        /// Append a pdf file from an xml entry to the copy handler
        /// and build bookmark entry.
        /// </summary>
        /// <param name="filePath">The pdf file to append to the pdf merger instance.</param>
        /// <param name="pdfMerger">The pdf merger instance.</param>
        /// <param name="numPages">The number of pages added.</param>
        /// <returns>true/false for success.</returns>
        private bool AppendDocument(string filePath, iText.Kernel.Utils.PdfMerger pdfMerger, out int numPages)
        {
            numPages = 0;

            // Make sure that the file path has been specified
            if (string.IsNullOrWhiteSpace(filePath))
            {
                Log.Error("Unsupported configuration file structure [element].");
                return false;
            }

            // Read the pdf file
            PdfDocument pdfDocument;
            try
            {
                pdfDocument = new PdfDocument(new PdfReader(filePath));

                // If the document currently has bookmarks (outlines),
                // we can choose to remove or keep them. Currently we
                // do not support moving them in the navigation pane, 
                // so they won't be grouped under the matching document
                // reference, though they still link correctly.
                if (!_initializationProperties.KeepCurrentPdfBookmarks)
                {
                    var currentOutlines = pdfDocument.GetOutlines(false);
                    if (currentOutlines != null)
                    {
                        var outlines = currentOutlines.GetAllChildren();
                        for (var i = 0; i < outlines.Count; i++)
                        {
                            outlines.RemoveAt(i);
                        }

                        pdfDocument.GetCatalog().GetPdfObject().Remove(PdfName.Outlines);
                        pdfDocument.GetOutlines(true); // TODO: Guess this is not needed, but requires further testing.
                    }
                }

                numPages = pdfDocument.GetNumberOfPages();
            }
            catch (iText.IO.IOException e)
            {
                Log.Error(e, "AppendDocument threw an exception while reading the pdf file {FilePath}.", filePath);
                return false;
            }
            
            // Try to append the document to the output stream
            try
            {
                pdfMerger.Merge(pdfDocument, 1, numPages);
            }
            catch (PdfException e)
            {
                Log.Error(e, "AppendDocument threw an exception while merging the pdf file {FilePath}.", filePath);
                pdfDocument.Close();
                return false;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Log.Error(
                    e,
                    "AppendDocument threw an exception while merging the pdf file {FilePath}. Number of pages {NumberOfPages} in the source document must be 1 or higher.",
                    filePath, numPages);
                pdfDocument.Close();
                return false;
            }

            // Close the reader
            pdfDocument.Close();

            Log.Information("{FilePath} was appended. Number of pages: {NumPages}", filePath, numPages);
            return true;
        }

        /// <summary>
        /// Append bookmark entry to the bookmarks collection.
        /// </summary>
        /// <param name="title">The title of the bookmark.</param>
        /// <param name="level">The tree/child level of the bookmark.</param>
        /// <param name="page">The page to set for the bookmark.</param>
        /// <returns>true/false for success.</returns>
        private bool AppendBookmark(string title, int level, int page)
        {
            // Skip if bookmarks are not enabled
            if (!_mergeProperties.EnableBookmarks)
            {
                return true;
            }

            // Traverse up in the tree to find the matching parent level:
            // - If levels are the same, GetParent is called once only.
            // - If level is lower than the last, GetParent is called a number of times.
            // - If level is higher than the last, this call will be skipped.
            for (var i = 0; i < _lastBookmarkLevel - level + 1; i++)
            {
                _lastBookmark = _lastBookmark.GetParent();
                if (null == _lastBookmark)
                {
                    Log.Error("Parent bookmark level was not found!");
                    return false;
                }
            }

            // Now add the bookmark
            _lastBookmark = _lastBookmark.AddOutline(title);
            _lastBookmark.AddDestination(PdfExplicitRemoteGoToDestination.CreateFit(page));
            _lastBookmarkLevel = level;

            Log.Information("Bookmark was appended. Title: {Title}, Level: {Level}, Page: {Page}.", title, level, page);
            return true;
        }

        /// <summary>
        /// Get merge properties from the xml merge entry.
        /// </summary>
        /// <param name="configurationFilePath">The path to the xml configuration file.</param>
        /// <param name="xmlElement">The xml configuration data.</param>
        /// <returns>true/false for success.</returns>
        private bool GetMergeProperties(string configurationFilePath, XmlElement xmlElement)
        {
            // Try to find the directory where the input files
            // are stored, in the case that file paths are not
            // specified as absolute. We will typically use this
            // only during debugging.
            var fileInfo = new FileInfo(configurationFilePath);
            var workingDirectory = fileInfo.DirectoryName;

            // Get destination file
            if (!xmlElement.HasAttribute("destinationFile"))
            {
                Log.Error("Unsupported configuration file structure [destinationFile].");
                return false;
            }
            var destinationFile = xmlElement.GetAttribute("destinationFile");
            
            // Make sure that the destination file is an absolute path.
            if (!destinationFile.Contains("\\") && 
                !string.IsNullOrWhiteSpace(workingDirectory))
            {
                destinationFile = Path.Combine(workingDirectory, destinationFile);
            }
            Log.Information("Destination file: {DestinationFile}", destinationFile);

            // Initialize _mergeProperties
            _mergeProperties = new MergeProperties
            {
                DestinationFile = destinationFile,
                WorkingDirectory = workingDirectory
            };

#if DEBUG
            try
            {
                File.Delete(_mergeProperties.DestinationFile);
                Log.Information("Debug mode: Deleted destination file.");
            }
            catch (Exception e)
            {
                // ignored
                Log.Error(e, "Debug mode: Failed to delete destination file.");
            }
#endif

            // Get title (if specified)
            if (xmlElement.HasAttribute("title"))
            {
                _mergeProperties.Title = xmlElement.GetAttribute("title");
            }

            // Get subject (if specified)
            if (xmlElement.HasAttribute("subject"))
            {
                _mergeProperties.Subject = xmlElement.GetAttribute("subject");
            }

            // Get author (if specified)
            if (xmlElement.HasAttribute("author"))
            {
                _mergeProperties.Author = xmlElement.GetAttribute("author");
            }

            // Get keywords (if specified)
            if (xmlElement.HasAttribute("keywords"))
            {
                _mergeProperties.Keywords = xmlElement.GetAttribute("keywords");
            }

            // Get enable PDF/A setting (if specified)
            if (xmlElement.HasAttribute("enablePdfA") &&
                bool.TryParse(xmlElement.GetAttribute("enablePdfA"), out var enablePdfA))
            {
                _mergeProperties.EnablePdfA = enablePdfA;
            }

            // Get enable bookmarks setting (if specified)
            if (xmlElement.HasAttribute("enableBookmarks") &&
                bool.TryParse(xmlElement.GetAttribute("enableBookmarks"), out var enableBookmarks))
            {
                _mergeProperties.EnableBookmarks = enableBookmarks;
            }
            
            Log.Information("Title: {Title}, Subject: {Subject}, Author: {Author}, Keywords: {Keywords}, EnablePdfA: {EnablePdfA}, EnableBookmarks: {EnableBookmarks}", 
                            _mergeProperties.Title, _mergeProperties.Subject, _mergeProperties.Author, _mergeProperties.Keywords, _mergeProperties.EnablePdfA, _mergeProperties.EnableBookmarks);
            return true;
        }

        /// <summary>
        /// Parse Pdf/A conformance level from the specified pdf file.
        /// </summary>
        /// <param name="filePath">The pdf file to get conformance level of.</param>
        /// <param name="conformanceLevel">The parsed Pdf/A conformance level.</param>
        /// <returns>true/false for success.</returns>
        private bool GetPdfAConformanceLevel(string filePath, out PdfAConformanceLevel conformanceLevel)
        {
            // Default level
            conformanceLevel = PdfAConformanceLevel.PDF_A_1A;

            // Make sure that the file path has been specified
            if (string.IsNullOrWhiteSpace(filePath))
            {
                Log.Error("Unsupported configuration file structure [element].");
                return false;
            }

            // Make sure that file path is absolute.
            if (!filePath.Contains("\\") &&
                !string.IsNullOrWhiteSpace(_mergeProperties.WorkingDirectory))
            {
                filePath = Path.Combine(_mergeProperties.WorkingDirectory, filePath);
            }

            try
            {
                // Unfortunately it seems that we need a PdfWriter
                // in order to get conformance level, though the 
                // PdfReader also included GetConformanceLevel.
                // We will not write anything, so the PdfWriter
                // simply points to an empty memory buffer, which
                // will be discarded automatically.
                var pdfDocument = new PdfADocument(new PdfReader(filePath), new PdfWriter(new MemoryStream()));
                conformanceLevel = pdfDocument.GetConformanceLevel();
                pdfDocument.Close();
            }
            catch (iText.IO.IOException e)
            {
                Log.Error(e, "GetPdfAConformanceLevel threw an exception while retrieving the pdf/a conformance level from the file {FilePath}.", filePath);
                return false;
            }

            Log.Information("PDF/A conformance level: {ConformanceLevel}", $"PDF/A-{conformanceLevel.GetPart()}{conformanceLevel.GetConformance()}");
            return true;
        }

    }
}
