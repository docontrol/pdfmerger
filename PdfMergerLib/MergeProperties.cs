﻿using System.Runtime.InteropServices;

namespace PdfMergerLib
{
    /// <summary>
    /// Merge properties read from the merge configuration file.
    /// </summary>
    [ComVisible(false)]
    public class MergeProperties
    {
        /// <summary>
        /// The destination/merged/output file.
        /// </summary>
        public string DestinationFile { get; set; }

        /// <summary>
        /// The pdf document title.
        /// </summary>
        public string Title { get; set; }

        /// The pdf document subject.
        public string Subject { get; set; }

        /// The pdf document author.
        public string Author { get; set; }

        /// The pdf document keywords.
        public string Keywords { get; set; }

        /// The pdf output will respect PDF/A.
        public bool EnablePdfA { get; set; }

        /// <summary>
        /// Add bookmarks to each file being merged in accordance with the supplied configuration.
        /// </summary>
        public bool EnableBookmarks { get; set; }

        /// <summary>
        /// The working directory where all files are stored.
        /// </summary>
        public string WorkingDirectory { get; set; }
    }
}
