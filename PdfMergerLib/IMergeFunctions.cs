﻿using System.Runtime.InteropServices;

namespace PdfMergerLib
{
    /// <summary>
    /// Interface for MergeFunctions.
    /// </summary>
    [Guid("4A5661D3-175E-442E-A9B3-5A478062CBDE")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [ComVisible(true)]
    public interface IMergeFunctions
    {
        /// <summary>
        /// Initialize using the supplied properties.
        /// </summary>
        /// <param name="initializationProperties">Initialization properties, e.g. program arguments in console application mode.</param>
        [ComVisible(false)]
        void Initialize(InitializationProperties initializationProperties);

        /// <summary>
        /// Initialize using the supplied properties.
        /// </summary>
        /// <param name="configurationFilePath">Configuration file path, e.g. C:\merge\config.xml or "C:\merge dir\config.xml".</param>
        /// <param name="logFilePath">Writes basic output log to this file. Use with Verbose mode to write a detailed log file.</param>
        /// <param name="compressLogFile">Apply GZIP compression to the log file (if any). The .gz extension will be appended automatically. Default is no compression.</param>
        /// <param name="verbose">Print verbose output. Default is no output aside from a final OK message. Use with log file to write detailed log.</param>
        /// <param name="keepCurrentPdfBookmarks">Keep bookmarks already present in the pdf files to merge. By default all current bookmarks are removed.</param>
        /// <param name="insertBlankPagesForDuplexPrinting">Insert blank pages for duplex printing purposes. Pdf files with an uneven number of pages will have have a blank page appended before the next file is appended.</param>
        void Initialize(string configurationFilePath, string logFilePath = null, bool compressLogFile = false, bool verbose = false,
                        bool keepCurrentPdfBookmarks = false, bool insertBlankPagesForDuplexPrinting = false);

        /// <summary>
        /// Merges pdf files based on an xml configuration file.
        /// </summary>
        /// <returns></returns>
        bool MergeUsingConfigurationFile();
    }
}