﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PdfMerger.test
{
    [TestClass]
    public class ConsoleTests
    {
        private readonly string _mergerApp;
        private readonly string _destinationFile;
        private readonly string _logFile;
        private readonly string _validConfigFile;
        private readonly string _validPdfAConfigFile;
        private readonly string _invalidXmlFile;

        public ConsoleTests()
        {
#if DEBUG
            _mergerApp = Path.GetFullPath("..\\..\\..\\PdfMerger\\bin\\Debug\\PdfMerger.exe");
#else
            _mergerApp = Path.GetFullPath("..\\..\\..\\PdfMerger\\bin\\Release\\PdfMerger.exe");
#endif
            _destinationFile = Path.GetFullPath("..\\..\\TestData\\merged.pdf");
            _logFile = Path.GetFullPath("..\\..\\TestData\\test.log");
            _validConfigFile = Path.GetFullPath("..\\..\\TestData\\valid.xml");
            _validPdfAConfigFile = Path.GetFullPath("..\\..\\TestData\\valid_pdfa.xml");
            _invalidXmlFile = Path.GetFullPath("..\\..\\TestData\\invalid.xml");
        }

        [TestCleanup]
        public void Cleanup()
        {
            try
            {
                if (File.Exists(_destinationFile)) { File.Delete(_destinationFile); }
                if (File.Exists(_logFile)) { File.Delete(_logFile); }
            }
            catch
            {
                // ignored
            }
        }

        public class ConsoleOutput : IDisposable
        {
            private readonly StringWriter _stringWriter;
            private readonly TextWriter _originalOutput;

            public ConsoleOutput()
            {
                _stringWriter = new StringWriter();
                _originalOutput = Console.Out;
                Console.SetOut(_stringWriter);
            }

            public string GetOutput()
            {
                return _stringWriter.ToString();
            }

            public void Dispose()
            {
                Console.SetOut(_originalOutput);
                _stringWriter.Dispose();
            }
        }

        /// <summary>
        /// Starts the console application.
        /// </summary>
        /// <param name="processPath">The absolute or relative path of the console application.</param>
        /// <param name="arguments">The arguments for console application. Specify empty string to run with no arguments</param>
        /// <returns>exit code</returns>
        private static int StartConsoleApplication(string processPath, string arguments)
        {
            // Initialize process here
            var proc = new Process
            {
                StartInfo =
                {
                    FileName = processPath,

                    // add arguments as whole string
                    Arguments = arguments,

                    // use it to start from testing environment
                    UseShellExecute = false,

                    // redirect outputs to have it in testing console
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,

                    // set working directory
                    WorkingDirectory = Environment.CurrentDirectory
                }
            };
            
            // start and wait for exit
            proc.Start();
            proc.WaitForExit();

            // get output to testing console.
            Console.WriteLine(proc.StandardOutput.ReadToEnd());
            Console.Write(proc.StandardError.ReadToEnd());

            // return exit code
            return proc.ExitCode;
        }

        [TestMethod]
        public void TestMissingArguments()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(1, StartConsoleApplication(_mergerApp, ""));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.IsTrue(output.Contains("Required option 'c, config' is missing."));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestInvalidArguments()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(1, StartConsoleApplication(_mergerApp, "xxx"));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.IsTrue(output.Contains("Required option 'c, config' is missing."));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestMissingConfigurationFile()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(1, StartConsoleApplication(_mergerApp, "-c xxx"));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.IsTrue(output.Contains("Error setting value to option 'c, config'"));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestInvalidConfigurationFile()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(1, StartConsoleApplication(_mergerApp, $"-c \"{_invalidXmlFile}\""));
                var output = consoleOutput.GetOutput(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.IsTrue(output.Contains("Unsupported configuration file structure"));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestValidConfigurationFile()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(0, StartConsoleApplication(_mergerApp, $"-c \"{_validConfigFile}\""));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.AreEqual("OK", output);
                Assert.IsTrue(File.Exists(_destinationFile));
                Assert.IsFalse(File.Exists(_logFile));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestValidPdfAConfigurationFile()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(0, StartConsoleApplication(_mergerApp, $"-c \"{_validPdfAConfigFile}\" -l \"{_logFile}\""));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.AreEqual("OK", output);
                Assert.IsTrue(File.Exists(_destinationFile));
                Assert.IsTrue(File.Exists(_logFile));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }

        [TestMethod]
        public void TestValidConfigurationFileWithLogOutput()
        {
            var currentConsoleOut = Console.Out;
            using (var consoleOutput = new ConsoleOutput())
            {
                // Check exit is normal
                Assert.AreEqual(0, StartConsoleApplication(_mergerApp, $"-c \"{_validConfigFile}\" -l \"{_logFile}\""));
                var output = consoleOutput.GetOutput().Trim(); // You did not specify the configuration file path. Simply supply the path as an argument using -config "<path>". Optionally specify -verbose to enable debug logging mode.
                Assert.AreEqual("OK", output);
                Assert.IsTrue(File.Exists(_destinationFile));
                Assert.IsTrue(File.Exists(_logFile));
            }
            Assert.AreEqual(currentConsoleOut, Console.Out);
        }
    }
}
