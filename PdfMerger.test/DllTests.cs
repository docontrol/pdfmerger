﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PdfMerger.test
{
    [TestClass]
    public class DllTests
    {
        private readonly string _destinationFile;
        private readonly string _logFile;
        private readonly string _validConfigFile;
        private readonly string _validPdfAConfigFile;
        private readonly string _invalidXmlFile;

        public DllTests()
        {
            _destinationFile = Path.GetFullPath("..\\..\\TestData\\merged.pdf");
            _logFile = Path.GetFullPath("..\\..\\TestData\\test.log");
            _validConfigFile = Path.GetFullPath("..\\..\\TestData\\valid.xml");
            _validPdfAConfigFile = Path.GetFullPath("..\\..\\TestData\\valid_pdfa.xml");
            _invalidXmlFile = Path.GetFullPath("..\\..\\TestData\\invalid.xml");
        }

        [TestCleanup]
        public void Cleanup()
        {
            try
            {
                if (File.Exists(_destinationFile)) { File.Delete(_destinationFile); }
                if (File.Exists(_logFile)) { File.Delete(_logFile); }
            }
            catch
            {
                // ignored
            }
        }

        [TestMethod]
        public void TestMethod1()
        {
            var mergeFunctions = new PdfMergerLib.MergeFunctions();
            mergeFunctions.Initialize(_validConfigFile, _logFile, false, true, false, true);
            var result = mergeFunctions.MergeUsingConfigurationFile();
            Assert.IsTrue(result);
        }
    }
}
