﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PdfMergerLib;

namespace PdfMerger.test
{
    [TestClass]
    public class MergeTests
    {
        private readonly string _destinationFile;
        private readonly ProgramArguments _programArguments;
        public MergeTests()
        {
            _destinationFile = "..\\..\\TestData\\merged.pdf";
            _programArguments = new ProgramArguments
            {
                LogFilePath = "..\\..\\TestData\\test.log",
                ConfigurationFilePath = "..\\..\\TestData\\valid.xml",
                KeepCurrentPdfBookmarks = false,
                Verbose = true,
                WriteLogFile = true
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            try
            {
                if (File.Exists(_destinationFile)) { File.Delete(_destinationFile); }
                if (File.Exists(_programArguments.LogFilePath)) { File.Delete(_programArguments.LogFilePath); }
                if (File.Exists(_programArguments.LogFilePath + ".gz")) { File.Delete(_programArguments.LogFilePath + ".gz"); }
            }
            catch
            {
                // ignored
            }
        }

        [TestMethod]
        public void TestValidConfigurationFileNoLog()
        {
            var mergeFunctions = new MergeFunctions();
            mergeFunctions.Initialize(_programArguments);
            Assert.IsTrue(mergeFunctions.MergeUsingConfigurationFile());
            Assert.IsTrue(File.Exists(_destinationFile));
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
        }

        [TestMethod]
        public void TestFailedInitialization()
        {
            var mergeFunctions = new MergeFunctions();
            Assert.IsFalse(mergeFunctions.MergeUsingConfigurationFile());
        }

        [TestMethod]
        public void TestValidConfigurationFileViaProgramMain()
        {
            Environment.ExitCode = 0;
            Program.Main(new []{"-c", _programArguments.ConfigurationFilePath, "-v", "-l", _programArguments.LogFilePath });
            Assert.IsTrue(File.Exists(_destinationFile));
            Assert.IsTrue(Environment.ExitCode == 0);
            if (_programArguments.WriteLogFile)
            {
                Assert.IsTrue(File.Exists(_programArguments.LogFilePath));
            }
            else
            {
                Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
            }
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
        }

        [TestMethod]
        public void TestInvalidConfigurationFileViaProgramMain()
        {
            Environment.ExitCode = 0;
            Program.Main(new []{"-c", "..\\..\\TestData\\invalid.xml", "-v", "-l", _programArguments.LogFilePath });
            Assert.IsTrue(Environment.ExitCode == 1);
            if (_programArguments.WriteLogFile)
            {
                Assert.IsTrue(File.Exists(_programArguments.LogFilePath));
            }
            else
            {
                Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
            }
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
        }

        [TestMethod]
        public void TestValidConfigurationFileForDuplexPrintingViaProgramMain()
        {
            Environment.ExitCode = 0;
            Program.Main(new []{"-c", _programArguments.ConfigurationFilePath, "-v", "-l", _programArguments.LogFilePath, "-i" });
            Assert.IsTrue(File.Exists(_destinationFile));
            Assert.IsTrue(Environment.ExitCode == 0);
            if (_programArguments.WriteLogFile)
            {
                Assert.IsTrue(File.Exists(_programArguments.LogFilePath));
            }
            else
            {
                Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
            }
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
        }

        [TestMethod]
        public void TestValidConfigurationFileWithCompressedLogViaProgramMain()
        {
            Environment.ExitCode = 0;
            Program.Main(new []{"-c", _programArguments.ConfigurationFilePath, "-v", "-l", _programArguments.LogFilePath, "-g" });
            Assert.IsTrue(File.Exists(_destinationFile));
            Assert.IsTrue(Environment.ExitCode == 0);
            if (_programArguments.WriteLogFile)
            {
                Assert.IsTrue(File.Exists(_programArguments.LogFilePath + ".gz"));
            }
            else
            {
                Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
            }
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
        }

        [TestMethod]
        public void TestValidConfigurationFileWithoutLogViaProgramMain()
        {
            Environment.ExitCode = 0;
            Program.Main(new []{"-c", _programArguments.ConfigurationFilePath, "-v" });
            Assert.IsTrue(File.Exists(_destinationFile));
            Assert.IsTrue(Environment.ExitCode == 0);
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath + ".gz"));
            Assert.IsFalse(File.Exists(_programArguments.LogFilePath));
        }
    }
}
