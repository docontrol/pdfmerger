﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommandLine;
using PdfMergerLib;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.File.GZip;

namespace PdfMerger
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Parser.Default.ParseArguments<ProgramArguments>(args)
                  .WithParsed(RunWithArguments)
                  .WithNotParsed(HandleParseError);
        }

        private static void RunWithArguments(ProgramArguments arguments)
        {
            // Setup logging
            // If verbose argument is enabled, output all logs to the console
            // otherwise only warnings and errors.
            var logConfig = new LoggerConfiguration()
                            .MinimumLevel.Verbose()
                            .WriteTo.Console(arguments.Verbose ? LogEventLevel.Verbose : LogEventLevel.Warning);
            
            // Only log to file if so requested.
            if (arguments.WriteLogFile)
            {
                var logFilePath = arguments.CompressLogFile ? arguments.LogFilePath + ".gz" : arguments.LogFilePath;
                logConfig.WriteTo.File(logFilePath, hooks: arguments.CompressLogFile ? new GZipHooks() : null);
            }
            Log.Logger = logConfig.CreateLogger();

            // Log the application name and version.
            Log.Information(System.Reflection.Assembly.GetExecutingAssembly().GetName().FullName);
            
            // Log the command line
            Log.Information("Command line: {CommandLine}", Parser.Default.FormatCommandLine(arguments));

            try
            {
                // Start merging
                var mergeFunctions = new MergeFunctions();
                mergeFunctions.Initialize(arguments);
                if (mergeFunctions.MergeUsingConfigurationFile())
                {
                    // Write OK to the console to indicate success result.
                    Console.WriteLine("OK");
                }
                else
                {
                    // Return exit code 1 on errors. Successful execution will return 0.
                    Environment.ExitCode = 1;
                }
            }
            catch (Exception e)
            {
                // Do not throw any exceptions from the application.
                // Instead output the generic exception.
                Log.Error(e, "An unexpected exception was thrown.");

                // Return exit code 1 on errors. Successful execution will return 0.
                Environment.ExitCode = 1;
            }

            // Release ressources used by the logger
            Log.CloseAndFlush();
        }

        private static void HandleParseError(IEnumerable<Error> errors)
        {
            if (!errors.Any(x => x is HelpRequestedError || x is VersionRequestedError))
            {
                Environment.ExitCode = 1;
            }
        }
    }
}
