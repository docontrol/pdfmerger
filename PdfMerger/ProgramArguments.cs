﻿using System;
using System.IO;
using CommandLine;
using PdfMergerLib;

namespace PdfMerger
{
    public class ProgramArguments : InitializationProperties
    {
        [Option('c', "config", Required = true, HelpText = "Configuration file path, e.g. C:\\merge\\config.xml or \"C:\\merge dir\\config.xml\".")]
        public override string ConfigurationFilePath
        {
            get => _configurationFilePath;
            set
            {
                // Make sure that we specified a valid path. Any exception thrown will be automatically caught
                // and an error message returned.
                try
                {
                    value = Path.GetFullPath(value);
                }
                catch (Exception)
                {
                    throw new ArgumentException("The specified configuration file path is not a valid.");
                }

                // Make sure that we specified a valid path. Any exception thrown will be automatically caught
                // and an error message returned.
                var info = new FileInfo(value);
                if (!info.Exists)
                {
                    throw new ArgumentException("The specified configuration file was not found.");
                }

                _configurationFilePath = value;
            }
        }
        private string _configurationFilePath;

        [Option('l', "logfile", Required = false, HelpText = "Writes basic output log to this file. Use with Verbose mode to write a detailed log file.")]
        public override string LogFilePath
        {
            get => _logFilePath;
            set
            {
                // Make sure that we specified a valid path. Any exception thrown will be automatically caught
                // and an error message returned.
                try
                {
                    value = Path.GetFullPath(value);
                }
                catch (Exception)
                {
                    throw new ArgumentException("The specified log file path is not a valid file path.");
                }
                
                _logFilePath = value;
                WriteLogFile = true;
            }
        }

        private string _logFilePath;
        public override bool WriteLogFile { get; set; }

        [Option('g', "compressLogFile", Default = false, HelpText = "Apply GZIP compression to the log file (if any). The .gz extension will be appended automatically. Default is no compression.")]
        public override bool CompressLogFile { get; set; }

        [Option('v', "verbose", Default = false, HelpText = "Print verbose output. Default is no output aside from a final OK message. Use with log file to write detailed log.")]
        public override bool Verbose { get; set; }
        
        [Option('k', 
                "keepCurrentPdfBookmarks", 
                Default = false, 
                HelpText = "Keep bookmarks already present in the pdf files to merge. By default all current bookmarks are removed.")]
        public override bool KeepCurrentPdfBookmarks { get; set; }

        [Option('i', 
                "insertBlankPagesForDuplexPrinting", 
                Default = false, 
                HelpText = "Insert blank pages for duplex printing purposes. Pdf files with an uneven number of pages will have have a blank page appended before the next file is appended.")]
        public override bool InsertBlankPagesForDuplexPrinting { get; set; }
    }
}
